#!/usr/bin/make -f

UPSTREAM_GIT := https://opendev.org/openstack/masakari.git
include /usr/share/openstack-pkg-tools/pkgos.make

%:
	dh $@ --buildsystem=pybuild --with python3,sphinxdoc

override_dh_auto_clean:
	rm -f debian/*.init debian/*.service debian/*.upstart
	rm -rf masakari.sqlite doc/source/_static/*.sample debian/po
	rm -f debian/masakari-api.postinst debian/masakari-api.config debian/masakari-common.postinst debian/masakari-common.config debian/masakari-common.postrm
	rm -rf build .stestr *.egg-info debian/*.templates
	find . -iname '*.pyc' -delete
	for i in $$(find . -type d -iname __pycache__) ; do rm -rf $$i ; done

override_dh_auto_build:
	/usr/share/openstack-pkg-tools/pkgos_insert_include pkgos_func masakari-api.postinst
	/usr/share/openstack-pkg-tools/pkgos_insert_include pkgos_func masakari-api.config
	/usr/share/openstack-pkg-tools/pkgos_insert_include pkgos_func masakari-common.postinst
	/usr/share/openstack-pkg-tools/pkgos_insert_include pkgos_func masakari-common.config
	/usr/share/openstack-pkg-tools/pkgos_insert_include pkgos_postrm masakari-common.postrm
	pkgos-merge-templates masakari-api masakari endpoint
	pkgos-merge-templates masakari-common masakari db rabbit ksat

override_dh_auto_test:
	echo "Do nothing..."

override_dh_auto_install:
	echo "Do nothing..."

override_dh_install:
	for i in $(PYTHON3S) ; do \
		python$$i setup.py install --root=debian/tmp --install-layout=deb ; \
	done

	rm -rf $(CURDIR)/debian/tmp/usr/etc

ifeq (,$(findstring nocheck, $(DEB_BUILD_OPTIONS)))
	pkgos-dh_auto_test --no-py2 'masakari\.tests\.unit\.(?!test_hacking\.HackingTestCase\.test_no_os_popen|objects\.test_objects\.TestObjectVersions\.test_versions)'
endif

	# Generate the masakari.conf config
	mkdir -p $(CURDIR)/debian/masakari-common/usr/share/masakari-common
	PYTHONPATH=$(CURDIR)/debian/tmp/usr/lib/python3/dist-packages oslo-config-generator \
		--output-file $(CURDIR)/debian/masakari-common/usr/share/masakari-common/masakari.conf \
		--wrap-width 140 \
		--namespace keystonemiddleware.auth_token
		--namespace masakari.conf \
		--namespace oslo.db \
		--namespace oslo.db.concurrency \
		--namespace oslo.log \
		--namespace oslo.messaging \
		--namespace oslo.middleware \
		--namespace oslo.policy \
		--namespace oslo.service.service \
		--namespace oslo.service.wsgi \
		--namespace oslo.versionedobjects
	pkgos-readd-keystone-authtoken-missing-options $(CURDIR)/debian/masakari-common/usr/share/masakari-common/masakari.conf keystone_authtoken masakari

	PYTHONPATH=$(CURDIR)/debian/tmp/usr/lib/python3/dist-packages oslo-config-generator \
		--output-file $(CURDIR)/debian/masakari-common/usr/share/masakari-common/masakari-custom-recovery-methods.conf \
		--wrap-width 140 \
		--namespace customized_recovery_flow_opts

	# Same with policy.conf
	mkdir -p $(CURDIR)/debian/masakari-common/etc/masakari/policy.d
	PYTHONPATH=$(CURDIR)/debian/tmp/usr/lib/python3/dist-packages oslopolicy-sample-generator \
		--output-file $(CURDIR)/debian/masakari-common/etc/masakari/policy.d/00_default_policy.yaml \
		--format yaml \
		--namespace masakari

	# Comment out deprecated policies
	sed -i 's/^"/#"/' $(CURDIR)/debian/masakari-common/etc/masakari/policy.d/00_default_policy.yaml

	# Use the policy.d folder
	pkgos-fix-config-default $(CURDIR)/debian/masakari-common/usr/share/masakari-common/masakari.conf oslo_policy policy_dirs /etc/masakari/policy.d

	dh_install
	rm -rf $(CURDIR)/debian/tmp/usr/etc
	dh_missing --fail-missing

#	# Installing the other config files.
#	for i in api-paste.ini ; do \
#		install -D -m 0664 etc/masakari/$$i $(CURDIR)/debian/masakari-common/usr/share/masakari-common/$$i ; \
#	done

override_dh_sphinxdoc:
ifeq (,$(findstring nodoc, $(DEB_BUILD_OPTIONS)))
	PYTHONPATH=. PYTHON=python3 python3 -m sphinx -b html doc/source $(CURDIR)/debian/masakari-doc/usr/share/doc/masakari-doc/html
	dh_sphinxdoc
endif

override_dh_python3:
	dh_python3 --shebang=/usr/bin/python3
